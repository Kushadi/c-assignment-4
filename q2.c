#include <stdio.h>
#include <math.h>
 
int main()
{
 
    float radius, height;
    float volume;
 
    printf("Enter value of radius and height of a cone :\n ");
    scanf("%f%f", &radius, &height);
    
    volume = (1.0/3) * (22 / 7) * radius * radius * height;
    
    printf("\n Volume of cone is : %.3f", volume);
    return 0;
}